package test;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.DemoFramework.browserutil.InitFramework;

public class test {
	InitFramework initdriver;
	test(){
		initdriver=InitFramework.initializeFramework();
}
	
public void  run() {
    //initdriver.loadLog4j();
	Logger log=Logger.getLogger(test.class.getName());
	initdriver.getBrowser("chrome");
	log.info("chrome is launch sucessfully");
    initdriver.driver.get("https://www.lenskart.com");
    initdriver.maximize();
    WebElement element=initdriver.driver.findElement(By.xpath(""));
    initdriver.moveToElement(element);
    
    initdriver.timeUtil(3);
    initdriver.screenshot();
    initdriver.driver.close();
   
}
public static void main(String args[]) {
	test test=new test();
	test.run();
  }
}
