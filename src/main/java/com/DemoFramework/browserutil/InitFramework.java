package com.DemoFramework.browserutil;

public class InitFramework extends BrowserUtil {
	public static InitFramework initdriver;
	private InitFramework(){}
	public static InitFramework initializeFramework() {
		if(initdriver==null) {
			initdriver=new InitFramework();
		}
		
		return initdriver;
		
	}

}
