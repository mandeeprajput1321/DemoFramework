package com.DemoFramework.browserutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserUtil {
	public WebDriver driver;
	public Properties property;
	public File file;
	
	public FileInputStream fis;
	public void getBrowser(String browsername) {
		if(System.getProperty("os.name").contains("Windows")) {
			if(browsername.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", ".\\src\\main\\resources\\drivers\\geckodriver.exe");
				driver=new FirefoxDriver();
			} 
			else if(browsername.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\drivers\\chromedriver.exe");
				driver=new ChromeDriver();
			}	
		}
		else if(System.getProperty("os.name").contains("Mac")) {
			if(browsername.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "\\src\\main\\resources\\drivers\\geckodriver");
				driver=new FirefoxDriver();
			} 

			else if(browsername.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "\\src\\main\\resources\\drivers\\chromedriver");
				driver=new ChromeDriver();
			}	 
		}
	}
	public void loadProperties() {
		try {
			property=new Properties();
			file=new File(".\\src\\main\\resources\\properties\\data.Properties");
			fis=new FileInputStream(file);
			property.load(fis);


			file=new File(".\\src\\main\\resources\\properties\\locator.properties");
			fis=new FileInputStream(file);
			property.load(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getPropertyData(String propName) {
		loadProperties();
		String prop= property.getProperty(propName);
		return prop;
	}
//	public WebElement getLocator(String PropName) {
//		String locator=getPropertyData(PropName);
//		String[] split=locator.split(":");
//		String locatorType=split[0];
//		String locatorValue=split[1];
//		System.out.println(locatorType);
//		System.out.println(locatorValue);
//		if(locatorType=="id") {
//			return driver.findElement(By.id(locatorValue));	
//		}
//		else if((locatorType=="className")||(locatorType=="class")) {
//			return driver.findElement(By.className(locatorValue));
//		}
//		else if((locatorType=="linkText")||(locatorType=="link")) {
//			return driver.findElement(By.linkText(locatorValue));
//		}
//		else if(locatorType=="Xpath") {
//			return driver.findElement(By.xpath(locatorValue));
//		}
//		return null;	
//	}
	public void waitOnVisiblityText(WebElement element,int seconds){
		WebDriverWait wait=new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(element));	
	}
	public void timeUtil(int seconds) {
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void selectByText(WebElement element,String text) {
		Select selct=new Select(element);
		selct.selectByVisibleText(text);                                           	
	}
	public void selectRandom(WebElement element) {
		 Select select=new Select( element);
		  List<WebElement>options= select.getOptions();
		  int size=options.size();
		  Random random=new Random();
		  int index=random.nextInt(size);
		  select.selectByIndex(index);
	}
	public void screenshot() {
		
		
		try {
			File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			SimpleDateFormat formator  =new SimpleDateFormat("dd-M-yyyy_hh_mm_ss");
			Date date = new Date();
		    String timeStamp=formator.format(date);
			File destination= new File(".\\src\\main\\resources\\screenshort\\screenshot"+timeStamp+".png");
			FileUtils.copyFile(src, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			}
	public void moveToElement(WebElement element) {
		Actions action=new Actions(driver);
		action.moveToElement(element).build().perform();
	}
	public void clickAction(WebElement element) {
		Actions action=new Actions(driver);
		action.moveToElement(element).click(element).build().perform();
	}
	public void clickUsingJs(WebElement element) {
		JavascriptExecutor execute=  (JavascriptExecutor)driver;
		execute.executeScript("arguments[0].click();",element);
	}
	public void maximize() {
		driver.manage().window().maximize();
	}
	public void loadLog4j(){
		try {
			System.out.println("Loading log4j Property");
			property= new Properties();
			file=new File(".\\src\\main\\resources\\properties\\log4j.properties");
			fis=new FileInputStream(file);
			property.load(fis);
			PropertyConfigurator.configure(property);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}


